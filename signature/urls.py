from django.conf.urls import url

from . import views

app_name = 'signature'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^download-signature/$', views.download_signature, name='download_signature'),
    url(r'^generate-signature/$', views.generate_signature, name='generate_signature'),
]
