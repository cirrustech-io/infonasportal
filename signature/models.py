from __future__ import unicode_literals

from django.db import models

# Create your models here.

from django.db import models


class EmployeeDetails(models.Model):
    name = models.CharField(max_length=200)
    emp_title = models.CharField(max_length=100)
    office_no = models.IntegerField(default=0)
    fax_no = models.IntegerField(default=0)
    mobile_no = models.IntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return unicode(self.name)


