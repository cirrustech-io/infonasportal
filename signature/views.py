from django.shortcuts import render
from django.template.loader import render_to_string
import json
import uuid
from django.conf import settings

# Create your views here.

from django.http import HttpResponse


def index(request):
    
    fields = ''
    return render(request, 'signature/index.html', {'fields': fields})


def generate_signature(request):

    if request.POST:
        POST_PARAM = request.POST

    user_id = POST_PARAM['name'].strip().title()# + str(uuid.uuid1()).split('-')[0]
    user_id, file_name = create_file_name(user_id)
    user_sign = render_to_string("signature/user_signature.htm", {"user_data":POST_PARAM})
    fd = open(file_name, 'w')
    fd.write(user_sign)
    fd.close()
    
    return HttpResponse(json.dumps({'user_sign': user_sign, 'file_name':file_name, 'user_id':user_id}), content_type="application/json")

def download_signature(request):
    if request.GET:
        user_id = request.GET.get('user_id')
        user_id, file_name = create_file_name(user_id)
        with open(file_name, 'rb') as cont:
            response = HttpResponse(cont.read(),content_type='application/html')
            response['Content-Disposition'] = 'filename='+user_id+'.htm'
            return response
        cont.closed

def create_file_name(user_id):

    if "_Infonas_Signature" in user_id:
        user_id = user_id.replace(" ", "_").replace("%20","_").strip()
    else:
        user_id = user_id.replace(" ", "_").replace("%20","_")+"_Infonas_Signature".strip()
    file_name = settings.STATICFILES_DIRS[0]+'/signature/user_signatures/'+user_id+'.htm'
    return user_id, file_name
